import React from 'react'
import { Route } from 'react-router-dom'
import PropTypes from 'prop-types'

import { AuthenticatedComponent } from 'pubsweet-client'

import Navigation from './Navigation'

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={props => (
      <div>
        <Navigation />
        <AuthenticatedComponent>
          <Component {...props} />
        </AuthenticatedComponent>
      </div>
    )}
  />
)

PrivateRoute.propTypes = {
  component: PropTypes.node.isRequired,
}

export default PrivateRoute
