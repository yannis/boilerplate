import React from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'

// Manage
import UsersManager from 'pubsweet-component-users-manager/UsersManagerContainer'
import TeamsManager from 'pubsweet-component-teams-manager/TeamsManagerContainer'

// Authentication
import Login from 'pubsweet-component-login/LoginContainer'
import Signup from 'pubsweet-component-signup/SignupContainer'
import PasswordReset from 'pubsweet-component-password-reset-frontend/PasswordReset'

import Dashboard from './components/Dashboard'
import PrivateRoute from './components/PrivateRoute'

export default (
  <Switch>
    <PrivateRoute component={Dashboard} exact path="/dashboard" />
    <PrivateRoute component={UsersManager} path="/users" />
    <PrivateRoute component={TeamsManager} path="/teams" />

    <Route component={Login} path="/login" />
    <Route component={Signup} path="/signup" />
    <Route component={PasswordReset} path="/password-reset" />

    <Redirect path="*" to="/dashboard" />
  </Switch>
)
